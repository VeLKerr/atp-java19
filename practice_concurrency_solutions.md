## Задача 1. Reader/writer
Есть база данных (класс), к ней могут обращаться разные потоки посредством вызовов методов read/write.
Запись может происходить только в случае, если никто не читает с бд, в противном случае записывающий поток ожидает.
Запись представляет собой логгирование и засыпание потока на определенное время. Чтение предполагает также логгирование и инкремент числа читателей.
Читатель и писатель представляют собой отдельные классы, которые принимают в конструктор бд и вызывают на ней методы чтения и записи соответственно.
Методы read/write в классе DataBase должны быть потокобезопасные, то есть читать могут несколько потоков одновременно, писать один поток, и то при условии, что никто не читает.

## Решение

Класс базы данных:

```java
package RW;

/**
 This class represents a database.  There are many
 competing threads wishing to read and write.  It is
 acceptable to have multiple processes reading at the
 same time, but if one thread is writing then no other
 process may either read or write.
 */
public class DataBase {
    private int readers; // number of active readers

    /**
     * Initializes this database.
     */
    public DataBase() {
        this.readers = 0;
    }

    /**
     * Read from this database.
     *
     * @param number Number of the reader.
     */
    public void read(int number) {
        synchronized (this) {
            this.readers++;
            System.out.println("Reader " + number + " starts reading.");
        }

        final int DELAY = 5000;
        try {
            Thread.sleep((int)(Math.random() * DELAY));
        }
        catch (InterruptedException e) {
        }

        synchronized (this) {
            System.out.println("Reader " + number + " stops reading.");
            this.readers--;
            if (this.readers == 0)
                this.notifyAll();

        }
    }

    /**
     Writes to this database.
¬
     @param number Number of the writer.
     */
    public synchronized void write(int number)
    {
        while (this.readers != 0)
        {
            try
            {
                this.wait();
            }
            catch (InterruptedException e) {}
        }
        System.out.println("Writer " + number + " starts writing.");

        final int DELAY = 5000;
        try
        {
            Thread.sleep((int) (Math.random() * DELAY));
        }
        catch (InterruptedException e) {}

        System.out.println("Writer " + number + " stops writing.");
        this.notifyAll();
    }

}
```

Класс писателя/читателя в бд:

```java
package RW;

/**
 * This class represents a reader.
 */
public class Reader extends Thread {
    private static int readers = 0; // number of readers

    private int number;
    private DataBase database;

    /**
     * Creates a Reader for the specified database.
     *
     * @param database database from which to be read.
     */
    public Reader(DataBase database) {
        this.database = database;
        this.number = Reader.readers++;
    }

    /**
     * Reads.
     */
    @Override
    public void run() {
//        while (true) {
            final int DELAY = 5;
            try {
                Thread.sleep((int)(Math.random() * DELAY));
            }
            catch (InterruptedException e) {
            }
            this.database.read(this.number);
//        }
    }
}
```

```java
package RW;

/**
 * This class represents a writer.
 */
public class Writer extends Thread {
    private static int writers = 0; // number of writers

    private int number;
    private DataBase database;

    /**
     * Creates a Writer for the specified database.
     *
     * @param database database to which to write.
     */
    public Writer(DataBase database) {
        this.database = database;
        this.number = Writer.writers++;
    }

    /**
     * Writes.
     */
    @Override
    public void run() {
//        while (true) {
            final int DELAY = 5;
            try {
                Thread.sleep((int)(Math.random() * DELAY));
            }
            catch (InterruptedException e) {
            }
            this.database.write(this.number);
//        }
    }
}
```

Как всё это запускать:
```java
package RW;

public class Main {
    /**
     * Creates the specified number of readers and writers and starts them.
     */
    public static void main(String[] args) {
        DataBase db = new DataBase();

        Reader reader = new Reader(db);
        Reader reader1 = new Reader(db);
        Reader reader2 = new Reader(db);

        Writer writer = new Writer(db);

        System.out.println("run readers");

        reader.start();
        reader1.start();
        reader2.start();

        System.out.println("run writer");
        writer.start();

    }
}
```
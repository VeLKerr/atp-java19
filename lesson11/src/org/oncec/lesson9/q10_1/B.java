package org.oncec.lesson9.q10_1;

/**
 * Created by velkerr on 24.04.19.
 */

class A{
    private int temp;

    public A() {
        this.temp = 0;
    }

    public int getTemp() {
        return temp;
    }
}

public class B {
    public static void main(String[] args) {
        A a = new A();
    }
}

#### Многопоточное программирование в Java.

В контексте многопоточного программирования можно выделить две сущности: процессы и потоки. 
* Процессы не имеют доступа друг к другу и не могут использовать общие данные. Для каждого процесса ОС создает так называемое 
«виртуальное адресное пространство», к которому процесс имеет прямой доступ. Это пространство принадлежит процессу, 
содержит только его данные и находится в полном его распоряжении.
* Один поток – это одна единица исполнения кода. Каждый поток последовательно выполняет инструкции процесса, 
которому он принадлежит, параллельно с другими потоками этого процесса. Выполнение потоков в параллельном режиме может быть реальное (когда отдельное ядро
выполняет отдельный процесс) и псевдопараллельное (когда на одном ядре происходит разделение выполнения нескольких процессов). 

### Возможности создания потоков в Java. 
* Реализовать интерфейс Runnable.
Класс, реализующий этот интерфейс
```java
class SideThread implements Runnable {
    public void run()        //Этот метод будет выполняться в побочном потоке
    {
        System.out.println("Hello from the side thread!");
    }
}
```

Точка входа в программу
```java
public class Main {
    public static void main(String[] args) {
        SideThread sideThread = new SideThread();

        Thread thread = new Thread(sideThread);

        thread.start();

        System.out.println("The main thread");
    }
}
```

* Создать потомка класса Thread и переопределить его метод run()
```java
class SideThread extends Thread {
    @Override
    public void run()    //Этот метод будет выполнен в побочном потоке
    {
        System.out.println("Hello from the side thread!");
    }
}
```

Запускаем отдельный поток из главного
```java
public class Main {

    public static void main(String[] args) {
        SideThread sideThread = new SideThread();
        sideThread.start();

        System.out.println("The main thread");
    }
}
```

После вызова SideThread главный поток продолжает своё выполнение. Инструкции после вызова start() будут выполнены параллельно с интрукциями в start().

При работе с потоками используются методы из java.lang:
* sleep(). Во время выполнения метода sleep() система перестает выделять потоку процессорное время, распределяя его между другими потоками.
* yield(). Заставляет процессор переключиться на обработку других потоков системы.
* join(). Один поток ждёт завершения выполнения другого. Например, чтобы главный поток подождал завершения побочного потока myThready, необходимо выполнить инструкцию myThready.join() в главном потоке. 
* isAlive(). Проверяет, работает ли поток.
* setDaemon(). Если завершился последний обычный поток процесса, и остались только потоки-демоны, то они будут принудительно завершены и выполнение процесса закончится. 
* getPriority()/setPriority(). Возвращает/задаёт приоритет потока.
* suspend(), приостанавливающий поток, и resume(), продолжающий выполнение потока, были объявлены устаревшими и их использование отныне крайне нежелательно. 
* stop(). Завершает поток, также нежелателен. Дело в том что поток может быть «убит» во время выполнения операции, обрыв которой на полуслове оставит некоторый объект в неправильном состоянии, что приведет к появлению трудноотлавливаемой и случайным образом возникающей ошибке.
* interrupt(). Прерывает поток. В случае, если на потоке вызваны методы wait, join или sleep, выбрасывается InterruptedException.
* start(). Запускает поток.

Все методы можно посмотреть в классе java.lang.Thread

Разберем программу, демонстрирующую параллельную работу потоков и её хаотичность.
Два потока спорят, что было первым - яйцо или курица.
Класс яйца:
```java
class EggVoice extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            try {
                sleep(1000);
            }
            catch (InterruptedException e) {
            }

            System.out.println("Egg!");
        }
    }
}
```

Класс курицы. Мониторит кто последним закончил спор. Тут применяются разобранные ранее методы из java.lang: поток ChickenVoice
смотрит жив ли второй сторонний поток. Если ChickenVoice закончил писать ,а EggVoice ещё работает - значит последниим будет вывод "Egg!".

```java
public class ChickenVoice extends Thread {
    EggVoice eggVoice;

    ChickenVoice(EggVoice eggVoice) {
        this.eggVoice = eggVoice;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            try {
                sleep(1000);
            }
            catch (InterruptedException e) {
            }

            System.out.println("Chicken!!");
        }

        if (eggVoice.isAlive())    //Если оппонент еще не сказал последнее слово
        {
            try {
                eggVoice.join();    //Подождать пока оппонент закончит высказываться.
            }
            catch (InterruptedException e) {
            }

            System.out.println("Egg was the first!");
        }
        else    //если оппонент уже закончил высказываться
            System.out.println("Chicken was the first!");
    }
}
```

Точка входа в программу.
```java
public class Main {

    static EggVoice eggOpinion;
    static ChickenVoice chickenOpinion;

    public static void main(String[] args) throws InterruptedException {
        eggOpinion = new EggVoice();
        chickenOpinion = new ChickenVoice(eggOpinion);

        System.out.println("Debate starts...");

        eggOpinion.start();
        chickenOpinion.start();

        eggOpinion.join();
        chickenOpinion.join();

        System.out.println("Debate is over!");
    }
}
```

#### Mutex в Java

Ограничить доступ к блоку кода одним потоком можно несколькими способами. Самый простой - используя ключевое слово synchronized. 
Его можно использовать на нескольких уровнях:
* статические методы
```java
public synchronized void synchronisedMethod() {
    // some logic
}
```
* обычные методы
```java
public static synchronized void synchronisedStaticMethod() {
    // some logic
}
```
* блоки кода
Во время синхронизации на объекте берётся так называемый монитор, Если монитор взят потоком, то доступ к объекту для других потоков закрыт.
Если монитор явно не указан в ключевом слове synchronised, то он берётся на экземпляре объекта, либо на самом объекте в случае статического метода.
```java
public static void performSyncTask(){
    synchronized () {
        // mutex logic
    }
}
```

### Задача 1. Reader/writer
Есть база данных (класс), к ней могут обращаться разные потоки посредством вызовов методов read/write.
Запись может происходить только в случае, если никто не читает с бд, в противном случае записывающий поток ожидает.
Запись представляет собой логгирование и засыпание потока на определенное время. Чтение предполагает также логгирование и инкремент числа читателей.
Читатель и писатель представляют собой отдельные классы, которые принимают в конструктор бд и вызывают на ней методы чтения и записи соответственно.
Методы read/write в классе DataBase должны быть потокобезопасные, то есть читать могут несколько потоков одновременно, писать один поток, и то при условии, что никто не читает.

#### Интерфейс для задачи.
```java
public interface DataBase {
    
    /**
     * Читает данные из бд. Под "читает" подразумевается следующее: вывод в консоль номера
     * читателя, засыпание на 5_000 млсек.
     * Одновременно читать из бд могут неограниченное количество потоков, но только если не происходит
     * запись.
     * 
     * @param number Номер читателя.
     */
    public void read(int number);

    /**
     * Читает данные в бд. Запись может осуществляться только при условии, что никто не читает, в противном случае
     * записывающий поток ожидает, пока бд не освободится. Под записью также подразумевается вывод в консоль
     * номера писателя, а также логгирование в консоль.
     * 
     * @param number Номер читателя.
     */
    public void write(int number);
}
```

Также следует реализовать следующие два класса:
```java
public class ReaderImpl extends Thread {
    /**
     * Creates a ReaderImpl for the specified database.
     *
     * @param database database from which to be read.
     */
    public ReaderImpl(DataBaseImpl database) {
    //
    }

    /**
     * Reads.
     */
    @Override
    public void run() {
    // read
    }
}
```

```java
public class WriterImpl extends Thread {
    /**
     * Creates a WriterImpl for the specified database.
     *
     * @param database database to which to write.
     */
    public WriterImpl(DataBaseImpl database) {
    //
    }

    /**
     * Writes.
     */
    @Override
    public void run() {
        //write
    }
}
```
Работа программы должна выглядеть следующим образом:
```java
public class Main {
    /**
     * Creates the specified number of readers and writers and starts them.
     */
    public static void main(String[] args) {
        DataBaseImpl db = new DataBaseImpl();

        ReaderImpl readerImpl = new ReaderImpl(db);
        ReaderImpl readerImpl1 = new ReaderImpl(db);
        ReaderImpl readerImpl2 = new ReaderImpl(db);

        WriterImpl writerImpl = new WriterImpl(db);

        System.out.println("run readers");

        readerImpl.start();
        readerImpl1.start();
        readerImpl2.start();

        System.out.println("run writer");
        writerImpl.start();

    }
}
```

Второй способ ограничения многопоточного доступа - это использование примитивов синхронизации из пакета java.util.concurrent.*. Они предоставляют наиболее гибкий функционал, позволяющий
манипулировать выполнением потоков в зависимости от определённых условий. 
Для ознакомления перейдём на статью с хабра (там красиво и с анимацией) - https://habr.com/en/post/277669/

### Задача 2.
Задача также на эмуляцию работы бд, только с использованием библиотеки java.util.concurrent.
Один класс производит "работу с бд"  - логирует факт работы и ненадолго засыпает. Другой класс выполняет эту работу в отдельном потоке.
Класс Main создаёт пул потоков, в котором запускается работа с бд через ExecutorService, а потом запускает каждый поток.
Задача иллюстрирует как можно использовать библиотеку java.util.concurrent.

Интерфейс для работы с бд:
```java
public class Resource {
    private Lock lock = new ReentrantLock();

    /**
     * Берет @link java.util.concurrent.locks.ReentrantLock и производит "работу" с бд: выводит на консоль текущий поток
     * и засыпает. Потом гарантированно логирует факт завершения работы и отпускает лок.
     */
    public void workWithDB() {
    }
}
```

Запуск в отдельном потоке. Создать класс
```java
public class ConcurrencyWork implements Runnable
```
который переопределяет run, в котором запускает работу с бд.

Создание пула потоков в методе Main.
```java
Resource resource = new Resource();
ExecutorService service = Executors.newFixedThreadPool(3);
...
//запуск потока. Нужно сделать в цикле
service.execute(new ConcurrencyWork(resource));
```
Потом нужно гарантированно закрыть сервис.

Задача 3. Корабли.

Есть транспортные корабли, которые подплывают к туннелю и далее плывут к причалам для погрузки разного рода товара.
Они проходят через узкий туннель где одновременно могут находиться только 5 кораблей. Под словом “подплывают к туннели” имеется ввиду то, 
что корабли должны откуда-то появляться. Их может быть ограниченное количество, то есть 10 или 100, а может быть бесконечное множество. Слово “Подплывают” назовем генератором кораблей.
Вид кораблей и их вместительность могут быть разными в зависимости от типа товаров, которые нужно загрузить на корабль. 
То есть для ТЗ я придумал 3 Типа кораблей (Хлеб, Банан и Одежда) и три вида вместительности 10, 50, 100 шт. товаров. 3 типа кораблей * 3 вида вместительности = 9 разных видов кораблей.
Далее есть 3 вида причалов для погрузки кораблей — Хлеб, Банан и Одежда. Каждый причал берет или подзывает к себе необходимый ему корабль и начинает его загружать. 
За одну секунду причал загружает на корабль 10 ед. товара. То есть если у корабля вместительность 50 шт., то причал загрузит его за 5 секунд своей работы.

Требования к задаче:

* Правильно разбить задачу на параллельность.
* Синхронизировать потоки, сохранить целостность данных. Ведь ограничить доступ потоков к общему ресурсу дело не сложное, а заставить их работать согласованно уже намного сложнее.
* Работа генератора кораблей не должна зависеть от работы причалов и наоборот.
* Общий ресурс должен быть Thread Safe (Если таковой есть в реализации)
* Потоки не должны быть активными если нет задач.
* Потоки не должны держать mutex если нет задач.

Класс корабль. Является потоком, в остальном обычный POJO-файл, содержит только информацию о корабле. 
```java
public class Ship extends Thread {
    private int count;
    private Size size;
    private Type type;

    //...
}
```
Также надо создать классы Size, Type для характеристики кораблей

```java
public enum Type {
    //перечисление типов
}
```

Обратите внимание, что в следующем enum классе можно задавать не только название, но и значение поля.
```java
public enum Size {
    SMALL(10), MIDDLE(50), LARGE(100);

    Size(int value){
        this.value = value;
    }
    private int value;

    public int getValue() {
        return value;
    }
}
```

Интерфейс Tunnel, который пропускает корабли и является узким горлышком задачи.
```java
public interface Tunnel {
    /**
     * Добавляет корабль в очередь через тоннель. Если количество кораблей в тоннеле больше, чем может быть, то
     * ставит поток на ожидание. При добавлении нового корабля, если место в тоннеле есть, будит все потоки.
     *
     * @param element
     * @return true если кораблю удалось войти в тоннель.
     * @return false, если места в тоннеле нет, и поток ставится на ожидание.
     */
    boolean add(Ship element);

    /**
     * Выводит корабль из туннеля определенного типа. Если в тоннеле нет кораблей, то вызываемый поток ставится на ожидание.
     * В противном случае происходит пробуждение всех потоков, проход по всем кораблям в тоннеле и возвращение корабля
     * определенного типа. Из внутренней структуры корабль должен быть удалён.
     *
     * @param shipType тип выводимого корабля.
     * @return выводимый корабль.
     */
    Ship get(Type shipType);
}
```
Генератор кораблей. Создаёт корабль и отправляет его в тоннель. В конструктор класса нужно передать соответсвующий класс
тоннеля и максимальное количество кораблей. Для обеспечения автономности работает в отдельном потоке, после генерации
очередного корабля засыпает на 1000 млсек.

```java
public class ShipGenerator implements Runnable {
    @Override
    public void run() {
//в цикле запускает создание кораблей и передачу их в тоннель
    }

//вспомогательные методы для генерации кораблей.
    private Type getRandomType() {
        Random random = new Random();
        return Type.values()[random.nextInt(Type.values().length)];
    }

    private Size getRandomSize() {
        Random random = new Random();
        return Size.values()[random.nextInt(Size.values().length)];
    }
}
```

Осталось создать причал. Он также должен быть автономным, так как каждый причал принимает корабли независимо.
```java
public class PierLoader implements Runnable {
    private Tunnel tunnel;
    private Type shipType;

    public PierLoader(Tunnel tunnel, Type shipType) {
        this.tunnel = tunnel;
        this.shipType = shipType;
    }

    @Override
    public void run() {

        while (true) {
            //берет из тоннеля корабль соответствующего типа 

            while (ship.countCheck()) {
                //загружает на него товар, пока корабль не наполнится
            }
        }
    }
}
```

Как в итоге должна выглядеть работа программы:
```java
public class Main {

    public static void main(String[] args) {
        System.out.println("Available number of cores: " + Runtime.getRuntime().availableProcessors());

        Tunnel tunnel = new Tunnel();

        ShipGenerator shipGenerator = new ShipGenerator(tunnel, 10);

        PierLoader pierLoader1 = new PierLoader(tunnel, Type.DRESS);
        PierLoader pierLoader2 = new PierLoader(tunnel, Type.BANANA);
        PierLoader pierLoader3 = new PierLoader(tunnel, Type.MEAL);

        ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        service.execute(shipGenerator);
        service.execute(pierLoader1);
        service.execute(pierLoader2);
        service.execute(pierLoader3);

        service.shutdown();
   }
}
```
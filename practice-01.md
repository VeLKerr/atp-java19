### Инструменты:
* [JDK v.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Intellij IDEA](https://www.jetbrains.com/idea/)
* Когда нет IDE, а хочется покодить: [compilejava.net](https://www.compilejava.net/)

### [Презентация](https://docs.google.com/presentation/d/1VT0DlhSoVmTWozhWE326GfKCuvUywOCuT94iHWPkQXQ/edit#slide=id.p)

### Программа, реализующая примитивный механизм часов.

``` java
// Часы принимают значения 0..23
// Минуты принимают значения 0..59
// Если при увеличении времени на 1 минуту получилось 60,
// выставить значение минуты в 0 и увеличить количество часов на 1.
// Если при увеличении часов получилось 24, выставить значение
// часов в 0.

public class Clock {
    private int hours;
    private int minutes;

    // Получить значение часов.
    public int getHours() {
    }

    // Получить значение минут.
    public int getMinutes() {
    }

    // Задать значение часов.
    public void setHours(int hours) {
    }

    // Задать значение минут.
    public void setMinutes(int minutes) {
    }

    // Увеличить значение минут на единицу.
    // Если значение минут превосходит 60, обнуляем счётчитк и увеличиваем 
    // значение часов на единицу.
    public void increaseMinutes() {
    }
}
```

##### Явное добавление конструктора в класс Clock.
``` java
    Clock (int minutes, int hours){
        this.minutes = minutes;
        
        this.hours = hours;
    }
```

##### Добавим в конструктор проверку на адекватность.
``` java
    Clock (int minutes, int hours) throws Exception {
        if(minutes < 0 || minutes > 60)
            throw new Exception("wrong minutes value");
        
        if(hours < 0 || hours > 23)
            throw new Exception("wrong hours value");
        
        this.minutes = minutes;

        this.hours = hours;
    }
```
* Переходим в класс Main и создаём экземпляр обёекта Clock с вышеуказанным конструктором.
* Теперь переопределим метод toString() в классе Clock. 
* Если этого не сделать, будет использован toString() класса-родителя, в данном случае Object.

``` java
    @Override public String toString() {
        return this.hours + "." + this.minutes;
    }
```
* Стоит отметить, что String является Immutable-объектом, что подразумевает только read-only доступ.
* Если изменить строку с помощью конатинации, то будет создана новая строка.
* Для того, чтобы оптимизировать работу с памятью, используются объекты StringBuilder && StringBuffer.
 
```java
    @Override public String toString() {
        StringBuilder res = new StringBuilder();
        res.append(minutes);
        res.append(".");
        res.append(hours);
        
        return res.toString();
    }
```


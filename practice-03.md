## Maven & Junit

Junit: https://javarush.ru/groups/posts/605-junit

Maven - инструмент автоматической сборки проектов. Необходим для компиляции всех файлов по всем директориям проекта, 
указания зависимостей на сторонние библиотеки.

#### Жизненный цикл Maven
Сборка maven соответствует специфическому жизненному циклу для развертывания и распространения проекта.
Выделяют три встроенных жизненных цикла:
* default: основной жизненный цикл, ответственный за развертывание проекта
* clean: чиста проекта и удаление всех файлов, сгенерированных в предыдущую сборку
* site: создание проектной документации

#### Фазы Maven

Фазы представляют шаги в жизненном цикле maven:

* validate — проверяет корректность метаинформации о проекте
* compile — компилирует исходники
* test — прогоняет тесты классов из предыдущего шага
* package — упаковывает скомпилированые классы в удобноперемещаемый формат (jar или war, к примеру)
* integration-test — отправляет упаковынные классы в среду интеграционного тестирования и прогоняет тесты
* verify — проверяет корректность пакета и удовлетворение требованиям качества
* install — загоняет пакет в локальный репозиторий, откуда он (пекат) будет доступен для использования как зависимость в других проектах
* deploy — отправляет пакет на удаленный production сервер, откуда другие разработчики его могут получить и использовать

При этом все шаги последовательны. И если, к примеру, выполнить $ mvn package, то фактически будут выполнены шаги: validate, compile, test и package. 
За более подробной информацией можно обратиться к официальной докумен ации Maven - http://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html#Lifecycle_Reference

#### POM file
Вся структура проекта описывается в pom файле. Он строится в формате xml.
Ключевым понятием Maven является артефакт — это любая библиотека, хранящаяся в репозитории. Это может быть какая-то зависимость или плагин. 
* Зависимости — это те библиотеки, которые непосредственно используются в вашем проекте для компиляции кода или его тестирования.
* Плагины используются самим Maven'ом при сборке проекта или для каких-то других целей (деплоймент, создание файлов проекта для IDEA и др.).
* Архетип — это некая стандартная компоновка файлов и каталогов в проектах различного рода (веб, swing-проекты и прочие). 
 Другими словами, Maven знает, как обычно строятся проекты и в соответствии с архетипом создает структуру каталогов.

#### Установка
На windows-системах maven придется устанавливать вручную. Последнюю версию maven'a можно скачать на официальной странице - http://maven.apache.org/download.cgi
Просто распаковываем архив в любую директорию. Далее необходимо создать переменную в Path, в которой необходимо указать путь к Maven.
Заходим в Win + Pause – Дополнительно – Переменные среды – в верхнем окошке нажимаем Создать, вводим имя M2_HOME и значение допустим “C:\apache-maven-2.2.1”. 
Далее там же создаем еще одну переменную M2 со значением %M2_HOME%\bin. Так же убеждаемся, что есть переменная JAVA_HOME с путем к JDK. 
Ее значение должно быть примерно таким «c:\Program Files\Java\jdk1.6.0_10\». И наконец в том же окошке создаем/модифицируем переменную Path, в нее необходимо просто написать 
%M2%, чтобы наша папочка с исполняемым файлом Maven была видна из командной строки. 

Проверить рабостоспособность можно команой ```mvn –version```. В результате должны получить похожий результат: 
```
Apache Maven 3.6.0 (97c98ec64a1fdfee7767ce5ffb20918da4f719f3; 2018-10-24T21:41:47+03:00)
Maven home: /usr/local/Cellar/maven/3.6.0/libexec
Java version: 1.8.0_152, vendor: Oracle Corporation, runtime: /Library/Java/JavaVirtualMachines/jdk1.8.0_152.jdk/Contents/Home/jre
Default locale: en_GB, platform encoding: UTF-8
OS name: "mac os x", version: "10.14.2", arch: "x86_64", family: "mac"
```

#### Сборка проекта
Выполняем ```mvn package``` или  ```mvn install```. Первая команда скомпилирует ваш проект и 
поместит его в папку target, а вторая еще и положит его к вам в локальный репозиторий. 

#### Зависимости
Все популярные библиотеки находятся в центральном репозитории - https://mvnrepository.com/repos/central, поэтому их можно прописывать сразу в раздел dependencies вашего pom-файла. 
Например чтобы подключить Spring Framework необходимо определить следующую зависимость:
```
<dependencies>
      ...
      <dependency>
         <groupId>org.springframework</groupId>
         <artifactId>spring</artifactId>
         <version>2.5.5</version>
      </dependency>
   </dependencies>
```

Но необходимой библиотеки в центральном репозитории может и не быть, в этом случае надо прописать требуемый репозиторий вручную
в файле settings.xml, который лежит в корне вашего локального репозитория, либо в самом файле pom.xml, в зависимости от того, 
нужен ли данный репозиторий во всех проектах, либо в каком-то одном конкретном, соответственно. Пример для JBoss RichFaces:
```
<!-- JBoss RichFaces Repository -->
   <repositories>
      <repository>
         <releases>
            <enabled>true</enabled>
         </releases>
         <snapshots>
            <enabled>false</enabled>
            <updatePolicy>never</updatePolicy>
         </snapshots>
         <id>repository.jboss.com</id>
         <name>Jboss Repository for Maven</name>
         <url>
            http://repository.jboss.com/maven2/
         </url>
         <layout>default</layout>
      </repository>
   </repositories>
```
#### Плагины
Так как плагины являются такими же артефактами, как и зависимости, то они описываются практически так же. 
Вместо раздела dependencies – plugins, dependency – plugin, repositories – pluginRepositories, repository – pluginRepository.

Плагинами Maven делает все, даже непосредственно то, для чего он затевался – сборку проекта, только этот плагин необязательно 
указывать в свойствах проекта, если вы не хотите добавить какие-то фичи.

NB. В среде разработки IDEA Maven уже предустановлен.

#### Запуск тестов под Maven.
Берём проект с предыдвщего занятия - https://javarush.ru/groups/posts/605-junit - и пробуем запустить тесты на Maven.
Сделать это можно командой ```mvn -Dtest=TestName test```


## JUnit 5
Остановимся на последней версии фреймворка тестирования JUnit.
У него присутствует полная обратная совместимость с предыдущей версией. В связи с этим архитектура фреймворка кардинально
изменилось: теперь JUnit состоит из нескольких модулей:
JUnit 5 = JUnit Platform + JUnit Jupiter + JUnit Vintage
* JUnit Platform. Представляет ядро для запуска тестовых фреймворков под JVM. Также определяется TestEngine для запуска тестов прямо из среды разработки.
* JUnit Jupiter. Новая модель программирования для написания тестов. Сопровождается новыми фичами.
* JUnit Vintage. Обеспечивает обратную совместимость с версией 4. 

#### Новые аннотации по сравнению с 4 версией:
* @TestFactory – метод для динамических тестов
* @DisplayName – выводит имя тестового метода
* @Nested – обозначает вложенный, нестатический класс
* @Tag – определяет тэги для фильтрации тестов
* @ExtendWith – используется для раширений
* @BeforeEach – аннотированный таким образом метод будет использоваться перед каждого тестового метода (JUnit4 @Before)
* @AfterEach – аннотированный таким образом метод будет использоваться после каждого тестового метода (JUnit4 @After)
* @BeforeAll – аннотированный таким образом метод будет использоваться перед всеми тестовыми методами (JUnit4 @BeforeClass)
* @AfterAll – аннотированный таким образом метод будет использоваться после всеми тестовыми методами (JUnit4 @AfterClass) 
* @Disable – игнорирование теста (JUnit4 @Ignore)
 
#### Тестирование исключений
Происходит с помощью метода ```assertThrows() ```
```java
@Test
void shouldThrowException() {
    Throwable exception = assertThrows(UnsupportedOperationException.class, () -> {
      throw new UnsupportedOperationException("Not supported");
    });
    assertEquals(exception.getMessage(), "Not supported");
}
 
@Test
void assertThrowsException() {
    String str = null;
    assertThrows(IllegalArgumentException.class, () -> {
      Integer.valueOf(str);
    });
}
```

Первый пример проверяет контекст исключений, в то время как второй валидирует тип исключения.

#### Test Suites
Данная парадигма подразумевает агрегирование тестовых классов в так называемые сьюты, чтобы их можно было запускать вместе.
JUnit5 предлагает следующие аннотации для создания сьют: ```@SelectPackages``` и ```@SelectClasses```.
Пример:
```java
@RunWith(JUnitPlatform.class)
@SelectPackages("com.hse")
public class AllTests {}
```

Также можно выбрать не целый пакет, а отдельные классы:
```
@RunWith(JUnitPlatform.class)
@SelectClasses({AssertionTest.class, AssumptionTest.class, ExceptionTest.class})
public class AllTests {}
```

#### Динамические тесты.
Позволяет объявлять и запускать тесты в run-time. В отличие от статических тестов, которые определяют классы
во время компиляции, динамические тесты позволяют определение в run-time.
На примере лямбда-выражений (которые более подробно будут рассмотрены позже):
```java
@TestFactory
public Stream<DynamicTest> translateDynamicTestsFromStream() {
    return in.stream()
      .map(word ->
          DynamicTest.dynamicTest("Test translate " + word, () -> {
            int id = in.indexOf(word);
            assertEquals(out.get(id), translate(word));
          })
    );
}
```
В предлагаемом ниже задании функционал лямбда-выражений будет использован минимально.

## Задание. Произвести миграцию проекта с JUnit4 на JUnit5.
Данное задание подкрепляет знания об инструменте сборки Maven и фреймворке JUnit 5.
Клонируйте к себе проект Maven под названием quckstart - https://github.com/1vanan/quickstart/tree/Junit5
Мы видим, что подключена зависимость JUnit 4:
```java
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
```


И плагин для мавена:
```java
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>8</source>
                    <target>8</target>
                </configuration>
            </plugin>
        </plugins>
```


Что необходимо сделать:
* подправить pom-файл. Добавить плагин surefire:
 ```java
             <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M3</version>
                <configuration>
                    <forkCount>1</forkCount>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.junit.jupiter</groupId>
                        <artifactId>junit-jupiter-engine</artifactId>
                        <version>5.3.2</version>
                    </dependency>
                </dependencies>
            </plugin>
```
* Удалить зависимость JUnit4  и добавить более новую версию:
```java
    <dependencies>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>5.3.2</version>
        </dependency>
        <dependency>
            <groupId>org.junit.vintage</groupId>
            <artifactId>junit-vintage-engine</artifactId>
            <version>5.3.2</version>
        </dependency>
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-launcher</artifactId>
            <version>1.3.2</version>
        </dependency>
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-runner</artifactId>
            <version>1.3.2</version>
        </dependency>
    </dependencies>
```
* поменять импорты: ```org.junit.Test``` --> ```org.junit.jupiter.api.Test```
 
```org.junit.Assert.assertTrue``` --> ```static org.junit.jupiter.api.Assertions.assertTrue```

Аналогично ```assertEquals```, ```assertNotNull```

* Изменить тестовую сьюту: 
```java
@RunWith(Suite.class)
@Suite.SuiteClasses({ApplicationTest.class, MainTest.class})
```
-->
```java
@RunWith(JUnitPlatform.class)
@SelectClasses({ApplicationTest.class, MainTest.class})
```
* Запустить несколько тестов динамическим способом:
```java
class MainSuiteTest {
    @TestFactory
    Collection<DynamicTest> testMain() {
        MainTest mainTest = new MainTest();
        return Arrays.asList(
            dynamicTest("test basic", () -> mainTest.testBasic()),
            dynamicTest("test main", () -> mainTest.testMain())
        );
    }
}
```
Таким образом можно запускать одинаковые тесты с разными параметрами.

* Также добавим в данном проекте следующий плагин:
```java
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>versions-maven-plugin</artifactId>
                <version>2.5</version>
                <configuration>
                    <generateBackupPoms>false</generateBackupPoms>
                </configuration>
            </plugin>
```
С его помощью можно мониторить версии используемых зависимостей в Maven-проекте.
Для этого нужно запустить команду: 
```mvn versions:display-dependency-updates```

#### Запуск тестов
Запустим тесты через чистый Maven: ```mvn -Dtest=ApplicationTest test```

Запуск целой сьюты: ```mvn -Dtest=MainSuiteTest test```
